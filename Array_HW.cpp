﻿#define _CRT_SECURE_NO_WARNINGS

#include <iostream>

#include <ctime> // получить текущее число календаря


const int i = 10;

time_t now = time(0); // текущая дата / время на основе текущей системы
tm* ltm = localtime(&now); // 

void ShowArray(int array[][i])
{
    for (int a = 0; a < i; a++)
    {
        for (int b = 0; b < i; b++)
        {
            std::cout << array[a][b] << " ";
        }
        std::cout << "\n";
    }
}

int main()
{
    /*
        Cоздать двумерный массив размерности NxN и заполнить его 
        таким образом, чтобы элемент с индексами i и j был равен i + j. 
    */



    int Array[i][i];
    //присваивание значений массива
    for (int a = 0; a < i; a++)
    {
        for (int b = 0; b < i; b++)
        {
            Array[a][b] = a + b;
        }
    }

    //Вывод массива на экран
    ShowArray(Array);


    /*
        Вывести сумму элементов в строке массива, 
        индекс которой равен остатку деления 
        текущего числа календаря на N. 
    */
    int summ = 0;
    int Num = 0;

    int ToDay = ltm->tm_mday;  // Получение текущего календарного числа
    std::cout << "ToDay: " << ToDay << "\n";
    
    Num = ToDay % i; // Номер строки которой равен остатку деления текущего числа календаря на N

    for (int a = 0; a < i; a++)
    {
        summ += Array[Num][a];
    }

    std::cout << "Sum of the numbers of the string with the index " << Num << " equals: " << summ << "\n";

    
}
